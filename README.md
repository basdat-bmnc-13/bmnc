Tugas Kelompok 4 Basis Data

Anggota :
- 1606838193 Dennis Febri Dien
- <tulisnpm> <tulisnama>
- 1606917600 Reza Ramadhansyah Putra

PERHATIAN
hanya boleh push master saat aplikasi sudah siap luncur.

gunakan branch 'dev' untuk peluncuran sementara aplikasi (untuk sementara belom terdeploy dimanapun)

setelah melakukan clone pada repo ini, lakukan perintah berikut untuk mendapatkan file dasar pengerjaan 

proyek basis data ini :

git checkout -b dev

git pull origin dev

setelah mendapatkan file dasar pengerjaan, lakukan pengerjaan file kalian di branch berbeda "TERHADAP DEV" (bukan terhadap master)
adapun perintahnya :

git checkout dev

git checkout -b nama_fitur_yang_mau_dibuat dev